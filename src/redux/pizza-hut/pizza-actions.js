
//Action
export const  BUY_PIZZA = 'BUY_PIZZA';

// Action Type
export const ActionType = {
    type:"string",
    payload:"string"
}

//Action creator
export const buyPizza = () => {
   return {
       type: BUY_PIZZA,
       payload : 'Chicken Pizza'
   }
}