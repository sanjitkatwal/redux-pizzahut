import * as pizzaAction from './pizza-actions'

// State type
// export const PizzaState ={
//     count : number
// }

//initial state
// let initialState.PizzaState = {
//     count: 55
// }

const initialState = 66

//reducer
const reducer = (state = initialState, action) => {
    switch (action.type){
        case pizzaAction.BUY_PIZZA:
            return{
                count:state.count - 1 > 0? state.count - 1 :0
            }
        default : return state
    }
};

export default reducer