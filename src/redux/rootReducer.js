// manager for all reducer

import {combineReducers} from 'redux';
import pizzaReducer from './pizza-hut/pizza.reducer';

 let rootReducer = combineReducers({
    pizza : pizzaReducer.reducer
});

export default rootReducer;