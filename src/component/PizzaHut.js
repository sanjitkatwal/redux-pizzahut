import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux'
import * as pizzaAction from '../redux/pizza-hut/pizza-actions';
import pizzaReducer from '../redux/pizza-hut/pizza.reducer'

let PizzaHut = () =>{

    // let [state, setState] = useState({
    //     count: 25
    // });

    let clickBuyPizza = () => {
        // read the pizza stat from redux


        // setState({
        //     count : state.count -1 > 0 ? state.count - 1 :0
        // })
    }
    return (
        <React.Fragment>

            <section className="mt-3">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <p className="h4 text-success">Pizza Hut</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam assumenda dolor eaque enim
                                fugit laudantium neque nostrum, nulla officiis omnis perspiciatis porro provident, quaerat
                                quidem quisquam quod repellendus ut velit!</p>
                        </div>
                    </div>
                </div>
            </section><section className="mt-3">
                <div className="container">
                    <div className="row">
                        <div className="col-md-5">
                            <img src={'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRG5vm_TDTjc64FO6hlWtL8TlWrG5HxdK5P3g&usqp=CAU'} alt={''} className="img-fluid"/>
                        </div>
                        <div className="col-md-7">
                            <p className="h3">Chicken Pizza</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam corporis dignissimos
                                doloribus ducimus fugiat ipsum nam nihil non odio, officia officiis provident quia rem
                                saepe sed tempore temporibus veniam voluptatibus?</p>
                            <p className="h2">Available:
                                <span className="fw-bold text-success"> {''}</span>
                            </p>
                            <button onClick={clickBuyPizza} className="btn btn-success btn-sm">Buy Pizza</button>
                        </div>
                    </div>
                </div>
            </section>

        </React.Fragment>
    )
}

export default PizzaHut